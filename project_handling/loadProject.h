#pragma once
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <fstream>
#include "../rapidxml/rapidxml.hpp"
#include "Project_settings.h"
/**
 * Projects always have the same folder stuctures and filenames
 * only the path changes
 */


/**
 * Load in the settings for the project into an instance of the settings type.
 * Includes the save path for the output mopfile
 */
settings  loadProjectSettings(path in){
  settings tmp;
  using namespace rapidxml;
xml_document<> doc; 
 xml_node<> *pNode;
 xml_node<> *pRoot;

std::ifstream file(in.projectCfgPath);
std::stringstream buffer;
buffer << file.rdbuf();
file.close();
std::string content(buffer.str());
doc.parse<0>(&content[0]);
//get the root node
 pRoot = doc.first_node();
 pNode = pRoot->first_node("G");
 //iterate through child nodes
 tmp.G = atof(pNode->value());
 pNode = pRoot->first_node("states");
 tmp.states = atoi(pNode->value());
 pNode = pRoot->first_node("steps");
 tmp.steps = atoi(pNode->value());
 pNode = pRoot->first_node("stepSize");
 tmp.stepSize = atoi(pNode->value());
 pNode = pRoot->first_node("mopName");
 tmp.mopName = pNode->value();
 tmp.projectOutMop = in.projectOutPath;
 tmp.projectOutMop.append(tmp.mopName +".mop");
return tmp;
}

