#pragma once
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <fstream>
#include "../rapidxml/rapidxml.hpp"
/**
 * Projects always have the same folder stuctures and filenames
 * only the path changes
 */

// the relevent type
typedef struct path {
  std::string projectName;
  std::string projectCfgPath;
  std::string projectParticlesPath;
  std::string projectOutPath;  
} path;

path loadPaths(std::string in) {
  path tmp;
  tmp.projectName.append(in);
  tmp.projectCfgPath.append(in);
  tmp.projectParticlesPath.append(in);
  tmp.projectOutPath.append(in);
  tmp.projectCfgPath.append("/cfg/cfg.xml");
  tmp.projectParticlesPath.append("/particles/particles.xml");
  tmp.projectOutPath.append("/result/");
  return tmp;
}
