#pragma once
#include <string>
/**
* This struct is used when manipulating/initialising the contents of a particle
*/
struct particle {
	/**
	* whether this particle is a collective (representing another node in a distributed architecture)
	*/
  bool isCollective;
	/**
	* the std::string name of this particle
	*/
  std::string name;

	/**
	* the id of this particle
	*/
  int id;
	/**
	* this particles type (collapsor or normal)
	*/
  int type;

	/**
	* particles colour expressed as an rgb int
	*/
	int r;
  	int g;
  	int b;

	/**
	*  what size this particle will have when displayed in a visualisation
	*/
	int size;
	/**
	* this particles mass
	*/
  double mass;
	/**
	* the radius for this particle
	*/
  double radius;

	/**
	* The position of this particle along the X axis
	*/
  double x;

	/**
	* The position of this particle along the Y axis
	*/
  double y;

	/**
	* The position of this particle along the Z axis
	*/
  double z;

	/**
	* The velocity of this particle along the X axis
	*/
  double xv;

	/**
	* The velocity of this particle along the Y axis
	*/
  double yv;

	/**
	* The velocity of this particle along the Z axis
	*/
  double zv;

	/**
	* The force acting on this particle along the X axis
	*/
  double xf;

	/**
	* The force acting on this particle along the Y axis
	*/
  double yf;

	/**
	* The force acting on this particle along the Z axis
	*/
	double zf;

	/**
	* X Axis location member that is read by other particles during integration
	*/
	double x_ext;

	/**
	* Y Axis location member that is read by other particles during integration
	*/
	double y_ext;

	/**
	* Z Axis location member that is read by other particles during integration
	*/
	double z_ext;
	/**
	* The stored initial position of this particle along the X axis
	*/
	double stored_x;

	/**
	* The stored initial position of this particle along the Y axis
	*/
	double stored_y;

	/**
	* The stored initial position of this particle along the Z axis
	*/
	double stored_z;

	/**
	* The stored initial velocity of this particle along the X axis
	*/
	double stored_xv;

	/**
	* The stored initial velocity of this particle along the Y axis
	*/
	double stored_yv;

	/**
	* The stored initial velocity of this particle along the Z axis
	*/
	double stored_zv;
  	/**
	*  What types of particle this particle is allowed to interact with.
	*/
	int interactionPermission;

  	/**
	*  what size this particle will have when displayed in a visualisation
	*/
	int visualRepresentation;
  	/**
	* what type this particle is - (collapsor/normal particle/placemark/spacecraft)
	*/
	int identity;

    /**
     * Particle interaction setting - Setting for all normal particles (planets and such) - interact with all particles, including special ones, except for placemarks
     */
    static const int interactALL = 1;
    
    /**
     * Particle interaction setting - Only interact with ordinary particles and collapsors (would be used for spacecraft)
     */
    static const int interactEnvironmentOnly = 2;
    
    /**
     * Particle interaction setting - Setting to be used if the particle is not to interact with any other particles (acting as a fixed placemark)
     */
    static const int interactNONE = 3;
    
    /**
     * Particle interaction setting - Setting to be used if the particle is not to interact with other particles of the same type (asteroids, spacecraft, things like that)
     */
    static const int interactDifferentOnly = 4;
    
    /**
     * Particle interaction setting - This particle is acting as a collapsor
     */
    static const int collapsor = 103;
    
    /**
     * Particle interaction setting - This particle is acting as a collapsor, and isn't being allowed to move
     */
    static const int collapsorFixed = 104;
    
    /**
     * Particle interaction setting -  A placemark particle, not to be subject to movement at all
     */
    static const int nonInteractive = 105;
    
    /**
     * Particle identity setting -  A Particle that needs no special treatment
     */
    static const int ordinary = 106;
    
    /**
     * Particle identity setting -  this particle is a very small body, so small that it needn't be made to interact with others of the same type.
     */
    static const int planetesimal = 108;

};
