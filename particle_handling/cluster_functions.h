#include "particle.h"
#include "ParamStruct.h"
#include "../glm/glm.hpp"
#include "../glm/gtc/random.hpp"

#include <cmath>
#include <string>
#include <sstream>

void startXMLFile(std::string);
void finishXMLFile(std::string);
void exportAsXMLToParticlesFile(std::string,particle);
    /**
    * generate a random number within the given range (double)
    */
    double randomRange(const double low, const double high) {
        double ret_val;
        double range = high-low;
        // generate a number between low..high
        ret_val = 0;
        while (ret_val==0) {
            ret_val =  (range*((rand()/(double) RAND_MAX)));
        }

        return low+ret_val;
    }

    /**
    * generate a random number within the given range (int)
    */
    int randomRangeInt( const int low,  const int high) {
        int ret_val;
        // generate a number between low..high
        ret_val = 0;
        while (ret_val==0) {
            ret_val =  rand() % high;
        }

        return low + ret_val;
    }

void makeGlob(std::string fileName, double minDistance, double maxDistance, double nMass,int mult, int pCount, int r,int g, int b) {
	double orbit_tmp;
	double curlyThing,theta;
	    particle content;
	int i;
    startXMLFile(fileName);

    for (i=0;i<pCount;i++) {
	    std::cout << "> Particle  "<< i <<std::endl;

		// place this particle at origin
		content.x = 0;
		content.y = 0;
		content.z = 0;

		//set some random mass
		content.mass =randomRange(nMass,nMass*mult); //double
	        std::cout << "> Particle mass "<< content.mass <<std::endl;

		content.radius = 2;
		content.id = i;
		content.visualRepresentation = randomRangeInt( 1, 30); //int
		content.name = "n";
		content.identity = 106;
		content.interactionPermission = 1;
		// The orbital height needs to have it's distance from origin added
		orbit_tmp = randomRange(minDistance,maxDistance);
		
		std::cout << "> Particle distance "<< orbit_tmp <<std::endl;

		// Pi is presented as an int here, but the result is divided, returning the result to its proper form
		curlyThing = ((double)(rand() % (2*3141592)))/100;
		theta = ((float)(rand() % 3141592))/100;
		// do all the rotation shit
		content.x += orbit_tmp*(cos(curlyThing)*sin(theta));
		content.y += orbit_tmp*(sin(curlyThing)*sin(theta));
		content.z += orbit_tmp*(cos(theta));
		content.r = r;
		content.g = g;
		content.b = b;
		content.xv = 0;
		content.yv = 0;
		content.zv = 0;
                exportAsXMLToParticlesFile(fileName,content);
    }
   finishXMLFile(fileName);
}


	//cluster is extended along y axis	
void makeTestSet(std::string fileName,int clusterTotal, int particleCount) {
        int xp=10;
        int yp=10;
	int zp = 10;
	int x,y;
	particle content;
        startXMLFile(fileName);
    for (x = 1;x <clusterTotal+1;x++) {
	  yp+=10;
	  zp+=10;
            for (y = 0; y<particleCount;y++) {
                content.mass =glm::linearRand(1.898600e+027,1.989100e+030);//(p.minMass,p.maxMass); //double
                content.radius = 1; // not sure what to do with this yet, as it's unused
		std::stringstream out;
                out <<"testSet_cluster_"<<x;
                content.name = out.str();
                content.id = x;
		content.visualRepresentation = 20;
                content.identity = particle::ordinary; //p.identity; 
                content.interactionPermission = particle::interactEnvironmentOnly;
                content.r = 191;//p.red;
                content.g = 244;//p.green;
                content.b = 66;//p.blue;
                content.xv = 0;
                content.yv = 0;
                content.zv = 0;
                content.x = xp;
                content.y = yp+y;
                content.z = zp+y;
		exportAsXMLToParticlesFile(fileName,content);
        }
     xp+=10;
    }
    finishXMLFile(fileName);

}





void startXMLFile(std::string fileName) {
        std::stringstream out;
	std::ofstream outFileStream;
        out <<"<root>" <<std::endl;
        std::string str2 = out.str();
        outFileStream.open(fileName.c_str(),std::ios::app);
        outFileStream << str2;
        outFileStream.close();
    }

void finishXMLFile(std::string fileName) {
        std::stringstream out;
	std::ofstream outFileStream;
        out <<"</root>" <<std::endl;
        std::string str2 = out.str();
        outFileStream.open(fileName.c_str(),std::ios::app);
        outFileStream << str2;
        outFileStream.close();
}
 
void exportAsXMLToParticlesFile(std::string fileName,particle content) {
        std::stringstream out;
	std::ofstream outFileStream;
        out <<"  <particle>" <<std::endl;
        out <<"   <interactionPermission>";
        if (content.interactionPermission == particle::interactALL) {
            out <<"interactALL";
        }
        if (content.interactionPermission == particle::interactNONE) {
            out <<"interactNONE";
        }
        if (content.interactionPermission == particle::interactEnvironmentOnly ) {
            out <<"interactEnvironmentOnly";
        }
        if (content.interactionPermission == particle::interactDifferentOnly) {
            out <<"interactDifferentOnly";
        }
        out <<"</interactionPermission>" <<std::endl;
        out <<"   <identity>";
        if (content.identity == particle::collapsor) {
            out <<"collapsor";
        }
        if (content.identity == particle::collapsorFixed) {
            out <<"collapsorFixed";
        }
        if (content.identity == particle::nonInteractive) {
            out <<"nonInteractive";
        }
        if (content.identity == particle::ordinary) {
            out <<"ordinary";
        }
        if (content.identity == particle::planetesimal) {
            out <<"planetesimal";
        }
        out <<"</identity>" <<std::endl;
 	out << " <id>"<< content.id<< "</id>" <<std::endl;
        out <<"   <name>"<< content.name<<"</name>" <<std::endl;
        out <<"   <visualSize>"<<content.visualRepresentation <<"</visualSize>" <<std::endl;
        out <<"   <rgb>" <<std::endl;
        out <<"    <red>"<<content.r <<"</red>" <<std::endl;
        out <<"    <green>"<<content.g <<"</green>" <<std::endl;
        out <<"    <blue>"<<content.b <<"</blue>" <<std::endl;
        out <<"   </rgb>" <<std::endl;
        out <<"   <mass>"<<std::scientific<< content.mass<<"</mass>" <<std::endl;
        out <<"   <radius>"<<std::scientific<< content.radius <<"</radius>" <<std::endl;
        out <<"   <vector>" <<std::endl;
        out <<"    <X>"<<std::scientific<<content.x <<"</X>" <<std::endl;
        out <<"    <Y>"<<std::scientific<<content.y <<"</Y>" <<std::endl;
        out <<"    <Z>"<<std::scientific<<content.z <<"</Z>" <<std::endl;
        out <<"    <XV>"<<std::scientific<<content.xv <<"</XV>" <<std::endl;
        out <<"    <YV>"<<std::scientific<<content.yv<<"</YV>" <<std::endl;
        out <<"    <ZV>"<<std::scientific<<content.zv <<"</ZV>" <<std::endl;
        out <<"   </vector>"<<std::endl;
        out <<"  </particle>" <<std::endl;
        std::string str2 = out.str();
        outFileStream.open(fileName.c_str(),std::ios::app);
        outFileStream << str2;
        outFileStream.close();

    }


/*
//cluster is extended along y axis	
	void makeCluster(std::ofstream outFileStream,std::string str,ParamStruct p) {
	double orbit_tmp;
	double pc;
	double pc2;
	double pc3;
	double tmp;
	double chance;
	double curlyThing,theta;
	Particle content;
        int tot = 1;
	double distanceThis;
	double distance2;

	while  (tot<pCount) {
		// place this particle at origin
		content.x = p.xin;
		content.y = p.yin;
		content.z = p.zin;
		//set some random mass
		content.mass =glm::linearRand(p.minMass,p.maxMass); //double
	        //std::cout << "> Particle mass "<< content.mass <<std::endl;
		content.radius = 1;
		content.id = tot;
		content.visualRepresentation = glm::linearRand( p.minRadius, p.maxRadius); //int
		content.name = "";
		content.identity = p.identity //106;
		content.interactionPermission = p.interactionPermission. 1;
		//straight line distance from mindistance to maxdistance
		orbit_tmp = glm::linearRand(0,p.distance);
	    // add the curve to the z axis via magic	       
		content.red = p.r;
		content.green = p.g;
		content.blue = p.b;
		content.xd = p.xd;
		content.yd = pd.yd;
		content.zd = p.zd;
		  //transform
		  // move to correct place
		content.x += p.xin;
		content.y += p.yin;
		content.z += p.zin;
		this->exportAsXMLToParticlesFile(std::ofstream outFileStream,std::string str,content);
		tot++;
		if (tot == pCount) {//done
		  break;
		}
    }
*/
