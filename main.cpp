/**
 * Author      : Dr Carey Pridgeon
 * Copyright   : Dr Carey Pridgeon 2016
 * Licence     : Licensed under the Apache License, Version 2.0 (the "License");
 *             : you may not use this file except in compliance with the License.
 *             : You may obtain a copy of the License at
 *             : http://www.apache.org/licenses/LICENSE-2.0
 *             :
 *             : Unless required by applicable law or agreed to in writing,
 *             : software distributed under the License is distributed on
 *             : an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 *             : either express or implied. See the License for the specific
 *             : language governing permissions and limitations under the License.
 */
 
#include "particleMash.h"
int main(int argc, char *argv[]) {
  //particleStore *set;
  path projectLoc;
  settings settingsStore;
  //projectLoc = loadPaths(argv[1]);
   //settingsStore = loadProjectSettings(projectLoc);
  //set = loadParticles(projectLoc);

  // generate a test set of five clusters
  //makeTestSet("testSet.xml",5,10);
  int particle_count = 1000;
  makeGlob("testSet_cluster.xml",0,9.461e+12,1.989100e+030,30,particle_count,191,244,66);
  
  
  
}




