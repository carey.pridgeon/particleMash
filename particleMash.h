/*
1;95;0c* Author      : Dr Carey Pridgeon
* Copyright   : Dr Carey Pridgeon 2010
* Licence     : Licensed under the Apache License, Version 2.0 (the "License");
*             : you may not use this file except in compliance with the License.
*             : You may obtain a copy of the License at
*             : http://www.apache.org/licenses/LICENSE-2.0
*             :
*             : Unless required by applicable law or agreed to in writing,
*             : software distributed under the License is distributed on
*             : an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
*             : either express or implied. See the License for the specific
*             : language governing permissions and limitations under the License.
*/
#pragma once
/**
	* include this in any program that needs to use Sulaco
	*/
#include <iostream>
#include <iomanip>
#include <exception>
#include "project_handling/loadPaths.h"
#include "project_handling/loadProject.h"
#include "particle_handling/loadParticles.h"
#include "particle_handling/stateStoring.h"
#include "particle_handling/stateChanging.h"
#include "particle_handling/cluster_functions.h"
path loadPaths(std::string);
settings loadProjectSettings( std::string);
particleStore  * loadParticles(path);
